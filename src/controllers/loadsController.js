const express = require('express');
const router = express.Router();

const {getLoadsByUserId, getLoadByIdForUser, addLoadToUser, updateLoadByIdForUser, postLoadByIdForUser, deleteLoadByIdForUser, getAssignedTruckByUserId, getAssignedLoadByUserId, switchAssignedLoadState} = require('../services/loadsService');
const {asyncWrapper} = require('../utils/apiUtils');
const {InvalidRequestError} = require('../utils/errors');
const {driveMiddleware} = require('../middlewares/driveMiddleware');
const {loadMiddleware} = require('../middlewares/loadMiddleware');

router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const {userId, role} = req.user;
      const {offset, limit, status} = req.query;

      const loads = await getLoadsByUserId(userId, role, offset, limit, status);

      res.json({loads});
    }),
);

router.post(
    '/',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      await addLoadToUser(userId, req.body);

      res.json({message: 'Load created successfully'});
    }),
);

router.get(
    '/active',
    driveMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      const load = await getAssignedLoadByUserId(userId);

      res.json({load});
    }),
);

router.patch(
    '/active/state',
    driveMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const newState = await switchAssignedLoadState(userId);
      res.json({message: `Load state changed to ${newState}`});
    }),
);

router.get(
    '/:id',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      const load = await getLoadByIdForUser(id, userId);

      if (!load) {
        throw new InvalidRequestError('No load with such id found!');
      }

      res.json({load});
    }),
);

router.put(
    '/:id',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const data = req.body;

      await updateLoadByIdForUser(userId, id, data);

      res.json({message: 'Load details changed successfullyc'});
    }),
);

router.delete(
    '/:id',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await deleteLoadByIdForUser(id, userId);

      res.json({message: 'Load deleted successfully'});
    }),
);

router.post(
    '/:id/post',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await postLoadByIdForUser(userId, id);

      res.json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }),
);

router.post(
    '/:id/shipping_info',
    loadMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const load = await getLoadByIdForUser(id, userId);

      if (!load) {
        throw new InvalidRequestError('No load with such id found!');
      }

      const truck = await getAssignedTruckByUserId(load.assigned_to);

      if (!truck) {
        throw new InvalidRequestError('No truck with such id found!');
      }

      res.json({load, truck});
    }),
);

module.exports = {
  loadsRouter: router,
};
