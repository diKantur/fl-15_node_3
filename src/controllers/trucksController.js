const express = require('express');
const router = express.Router();

const {getTrucksByUserId, getTruckByIdForUser, addTruckToUser, updateTruckByIdForUser, assignTruckByIdForUser, deleteTruckByIdForUser} = require('../services/trucksService');

const {asyncWrapper} = require('../utils/apiUtils');
const {InvalidRequestError} = require('../utils/errors');

router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {offset, limit} = req.query;

      const trucks = await getTrucksByUserId(userId, offset, limit);

      res.json({trucks});
    }),
);

router.post(
    '/',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      await addTruckToUser(userId, req.body);

      res.json({message: 'Truck created successfully'});
    }),
);

router.get(
    '/:id',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      const truck = await getTruckByIdForUser(id, userId);

      if (!truck) {
        throw new InvalidRequestError('No truck with such id found!');
      }

      res.json({truck});
    }),
);

router.put(
    '/:id',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const data = req.body;

      await updateTruckByIdForUser(userId, id, data);

      res.json({message: 'Truck details changed successfully'});
    }),
);

router.delete(
    '/:id',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await deleteTruckByIdForUser(id, userId);

      res.json({message: 'Truck deleted successfully'});
    }),
);

router.post(
    '/:id/assign',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;

      await assignTruckByIdForUser(userId, id);

      res.json({message: 'Truck assigned successfully'});
    }),
);

module.exports = {
  trucksRouter: router,
};
