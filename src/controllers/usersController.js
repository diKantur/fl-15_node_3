const express = require('express');
const router = express.Router();

const {getUserByUserId, deleteProfileByIdForUser, changePasswordByIdForUser} = require('../services/usersService');

const {asyncWrapper} = require('../utils/apiUtils');

router.get(
    '',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const user = await getUserByUserId(userId);

      res.json({user});
    }),
);

router.delete(
    '',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      await deleteProfileByIdForUser(userId);

      res.json({message: 'Profile deleted successfully'});
    }),
);

router.patch(
    '/password',
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {oldPassword, newPassword} = req.body;

      await changePasswordByIdForUser(userId, oldPassword, newPassword);

      res.json({message: 'Password changed successfully'});
    }),
);

module.exports = {
  usersRouter: router,
};
