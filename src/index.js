const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config();

const {trucksRouter} = require('./controllers/trucksController');
const {loadsRouter} = require('./controllers/loadsController');
const {usersRouter} = require('./controllers/usersController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {driveMiddleware} = require('./middlewares/driveMiddleware');
const {NodeCourseError} = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', [authMiddleware, driveMiddleware], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);

app.use((err, req, res, next) => {
  res.status(400).json({message: err.message});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(process.env.DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
