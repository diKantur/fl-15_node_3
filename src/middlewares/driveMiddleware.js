const driveMiddleware = (req, res, next) => {
  try {
    if (req.user.role === 'DRIVER') {
      next();
    } else {
      throw new Error('driveMiddleware error');
    }
  } catch (error) {
    next(error);
  }
};

module.exports = {
  driveMiddleware,
};
