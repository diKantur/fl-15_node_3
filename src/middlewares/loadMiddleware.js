const loadMiddleware = (req, res, next) => {
  try {
    if (req.user.role === 'SHIPPER') {
      next();
    } else {
      throw new Error('loadMiddleware error');
    }
  } catch (error) {
    next(error);
  }
};

module.exports = {
  loadMiddleware,
};
