const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: mongoose.Schema.Types.ObjectId,
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    default: null,
  },
  name: String,
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  logs: {
    type: [
      {
        message: String,
        time: Date,
      },
    ],
    default: [],
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Load};
