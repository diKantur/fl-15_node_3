const mongoose = require('mongoose');

const User = mongoose.model('User', {
  password: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    unique: true,
    required: true,
  },

  role: String,

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {User};
