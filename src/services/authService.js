const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });
  await user.save();
};

const signIn = async ({email, password, role}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign(
      {
        _id: user._id,
        role: user.role,
      },
      'secret',
  );
  return token;
};

const sendMail = async ({email}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new Error('Invalid email');
  }
  return;
};

module.exports = {
  registration,
  signIn,
  sendMail,
};
