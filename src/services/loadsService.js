const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

const getLoadsByUserId = async (userId, role, offset = '0', limit = '0', status) => {
  const loads = role === 'SHIPPER' ?
  await Load.find({created_by: userId}, '-__v').skip(parseInt(offset, 10)).limit(parseInt(limit, 10)) :
  await Load.find({assigned_to: userId}, '-__v').skip(parseInt(offset, 10)).limit(parseInt(limit, 10));

  return status ? loads.filter((v) => v.status === status) : loads;
};

const getLoadByIdForUser = async (loadId, userId) => {
  const load = await Load.findOne({_id: loadId, created_by: userId}, '-__v');
  return load;
};

const addLoadToUser = async (userId, loadPayload) => {
  const load = new Load({...loadPayload, created_by: userId});
  await load.save();
};

const updateLoadByIdForUser = async (userId, loadId, data) => {
  await Load.findOneAndUpdate({_id: loadId, created_by: userId}, {$set: data});
};

const postLoadByIdForUser = async (userId, loadId) => {
  const load = await Load.findOneAndUpdate({_id: loadId, created_by: userId}, {$set: {status: 'POSTED'}});

  await lookingForFreeTruck(load, userId);


  async function lookingForFreeTruck(load, userId) {
    const list = await Truck.find({status: 'IS'});

    filterListOfTruck(list, {payload: load.payload, dimensions: load.dimensions});

    if (!list[0]) {
      const load = await Load.findOneAndUpdate({_id: loadId, created_by: userId}, {$set: {status: 'NEW'}});

      load.logs[load.logs.length] = {
        message: 'truck was not assigned',
        time: Date.now(),
      };

      await Load.updateOne({$set: {logs: load.logs}});

      throw new Error('driver not found');
    }

    const founded = onFoundingFreeTruck(list[0]);

    return founded;
  }
  async function onFoundingFreeTruck(list) {
    const assigned = await Truck.findOneAndUpdate({_id: list._id}, {$set: {status: 'OL'}});

    if (!assigned) {
      throw new Error('do not assigned');
    }
    load.logs[load.logs.length] = {
      message: `Load assigned to driver with id ${list._id}`,
      time: Date.now(),
    };

    await Load.updateOne(
        {_id: loadId, created_by: userId},
        {
          $set: {
            status: 'ASSIGNED',
            logs: load.logs,
            state: 'En route to Pick Up',
            assigned_to: assigned.assigned_to,
          },
        },
    );
  }
  function filterListOfTruck(list, loadParams) {
    list.filter((v) => {
      const truckParams = getTruckParams(v.type);
      for (const key in loadParams.dimensions) {
        if (Object.hasOwnProperty.call(loadParams.dimensions, key)) {
          if (truckParams.dimensions[key] < loadParams.dimensions[key]) {
            return false;
          }
        }
      }
      return truckParams.payload > loadParams.payload ? true : false;
    });

    function getTruckParams(type) {
      switch (type) {
        case 'SPRINTER':
          return {
            payload: 1700,
            dimensions: {
              width: 300,
              length: 250,
              height: 170,
            },
          };
        case 'SMALL STRAIGHT':
          return {
            payload: 2500,
            dimensions: {
              width: 500,
              length: 250,
              height: 170,
            },
          };
        case 'LARGE STRAIGHT':
          return {
            payload: 4000,
            dimensions: {
              width: 700,
              length: 350,
              height: 200,
            },
          };

        default:
          throw new Error('type not found');
      }
    }
  }
};

const deleteLoadByIdForUser = async (loadId, userId) => {
  await Load.findOneAndRemove({_id: loadId, created_by: userId});
};

const getAssignedTruckByUserId = async (userId) => {
  return await Truck.findOne({assigned_to: userId}, '-__v');
};

const getAssignedLoadByUserId = async (userId) => {
  return await Load.findOne({assigned_to: userId}, '-__v');
};

const switchAssignedLoadState = async (userId) => {
  const load = await Load.findOne({assigned_to: userId}, '-__v');

  const newState = stateSwitcher(load.state);

  await Load.findOneAndUpdate({assigned_to: userId}, {$set: {state: newState}});

  if (newState === 'Arrived to delivery') {
    await Load.findOneAndUpdate({assigned_to: userId}, {$set: {status: 'SHIPPED'}});
    await Truck.findOneAndUpdate({assigned_to: userId}, {$set: {status: 'IS'}});
  }

  return newState;

  function stateSwitcher(param) {
    switch (param) {
      case 'En route to Pick Up':
        return 'Arrived to Pick Up';
      case 'Arrived to Pick Up':
        return 'En route to delivery';

      case 'En route to delivery':
        return 'Arrived to delivery';

      default:
        throw Error('some error');
    }
  }
};

module.exports = {
  getLoadsByUserId,
  getLoadByIdForUser,
  addLoadToUser,
  updateLoadByIdForUser,
  postLoadByIdForUser,
  deleteLoadByIdForUser,
  getAssignedTruckByUserId,
  getAssignedLoadByUserId,
  switchAssignedLoadState,
};
