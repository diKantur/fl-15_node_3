const {Truck} = require('../models/truckModel');

const getTrucksByUserId = async (userId, offset = '0', limit = '0') => {
  const trucks = await Truck.find({created_by: userId}, '-__v').skip(parseInt(offset, 10)).limit(parseInt(limit, 10));
  return trucks;
};

const getTruckByIdForUser = async (truckId, userId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: userId}, '-__v');
  return truck;
};

const addTruckToUser = async (userId, truckPayload) => {
  const truck = new Truck({...truckPayload, created_by: userId});
  await truck.save();
};

const updateTruckByIdForUser = async (userId, truckId, data) => {
  await Truck.findOneAndUpdate({_id: truckId, created_by: userId}, {$set: data});
};

const assignTruckByIdForUser = async (userId, truckId) => {
  const assigned = await Truck.findOne({assigned_to: userId});
  if (assigned === null) {
    await Truck.findOneAndUpdate({_id: truckId, created_by: userId}, {$set: {assigned_to: userId, status: 'IS'}});
  }
};

const deleteTruckByIdForUser = async (truckId, userId) => {
  await Truck.findOneAndRemove({_id: truckId, created_by: userId});
};

module.exports = {
  getTrucksByUserId,
  getTruckByIdForUser,
  addTruckToUser,
  updateTruckByIdForUser,
  assignTruckByIdForUser,
  deleteTruckByIdForUser,
};
